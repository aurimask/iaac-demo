Infrastructure as a code practice

Following code finished with the book's chapter 6.

To try it out you need to:

1. Initialize S3
2. Move S3 state to S3 bucket
3. Deploy DB from Stage folder
4. Deploy as many environments as needed from app module
