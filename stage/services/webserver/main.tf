provider "aws" {
  region ="eu-west-1"
}

module "app" {
  source = "../../../modules/services/app"
  environment = "Staging"
  instance_type = "t2.micro"
  min_size = 2
  max_size = 2
  server_port = 80

  app_remote_state_key ="stage/web/terraform.tfstate"

  custom_tags = {
    Owner = "Aurimas"
    DeployedBy = "Terraform"
  }
}
