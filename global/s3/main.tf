provider "aws" {
  region ="eu-west-1"
}

resource "aws_s3_bucket" "garazas_terraform_state" {
    bucket = "garazas-terraform-state"
    force_destroy = true
    lifecycle {
        prevent_destroy = false
    }
    versioning {
        enabled = true
    }
    server_side_encryption_configuration {
        rule {
            apply_server_side_encryption_by_default {
                sse_algorithm = "AES256"
            }
        }
    }
}

resource "aws_dynamodb_table" "terraform_locks" {
    name = "terraform-locks"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"

    attribute {
        name = "LockID"
        type = "S"
    }

}

/*terraform {
    backend "s3" {
        bucket = "garazas-terraform-state"
        key = "global/s3/terraform.tfstate" #file path
        region = "eu-west-1"
        dynamodb_table = "terraform-locks"
        encrypt = true
    }
} */
