provider "aws" {
  version = "~> 2.0"
  region ="eu-west-1"
}

resource "aws_db_instance" "terraform_database" {
    identifier_prefix = "terraformdb"
    skip_final_snapshot = true
    engine = "mysql"
    allocated_storage = 10
    instance_class = "db.t2.micro"
    name = "terraform_database"
    username = "admin"
    password = "Password123"
}

terraform {
    required_version = "=0.12.24"
    backend "s3" {
        bucket = "garazas-terraform-state"
        key = "stage/data/terraform.tfstate" #file path
        region = "eu-west-1"
        dynamodb_table = "terraform-locks"
        encrypt = true
    }
}
