provider "aws" {
  region ="eu-west-1"
}

module "app" {
  source = "../../../modules/services/app"
  environment = "Production"
  instance_type = "t2.micro"
  min_size = 3
  max_size = 3
  server_port = 80

  app_remote_state_key ="production/web/terraform.tfstate"

  custom_tags = {
    Owner = "Aurimas"
    DeployedBy = "Terraform"
  }
}
