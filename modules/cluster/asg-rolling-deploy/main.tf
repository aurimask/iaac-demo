provider "aws" {
  version = "~> 2.0"
  region ="eu-west-1"
}

locals {
  http_port = 80
  any_port = 0
  any_protocol = "-1"
  tcp_protocol = "tcp"
  all_ips = ["0.0.0.0/0"]
  server_port = 80
}

resource "aws_launch_configuration" "terraform-test" {
  image_id = "ami-035966e8adab4aaad"
  instance_type = var.instance_type
  security_groups = [aws_security_group.sg-terraform-test.id]

  user_data = <<-EOF
              #!/bin/bash
              echo "Laba diena" > index.html
              echo "${data.terraform_remote_state.db.outputs.address}" > index.html
              nohup busybox httpd -f -p ${local.server_port}
              &
              EOF
 lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "terraform-asg" {
  name = "${var.cluster_name}-${aws_launch_configuration.terraform-test.name}"
  launch_configuration = aws_launch_configuration.terraform-test.name
  vpc_zone_identifier = var.subnet_ids
  target_group_arns = var.target_group_arns
  health_check_type = var.health_check_type
  min_size = var.min_size
  max_size = var.max_size

  tag {
    key = "Name"
    value = var.cluster_name
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.custom_tags
    content {
      key = tag.key
      value = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_autoscaling_schedule" "scale_out_during_business_hours"{
    scheduled_action_name = "scale-out-during-business-hours"
    min_size = 2
    max_size = 10
    desired_capacity = 10
    recurrence = "0 9 * * *"
    autoscaling_group_name = aws_autoscaling_group.terraform-asg.name
}

resource "aws_autoscaling_schedule" "scale_in_at_night"{
    scheduled_action_name = "scale-in-at-night"
    min_size = 2
    max_size = 10
    desired_capacity = 2
    recurrence = "0 17 * * *"
    autoscaling_group_name = aws_autoscaling_group.terraform-asg.name
}

resource "aws_security_group" "sg-terraform-test" {
  name = "${var.cluster_name}-sg"
}

resource "aws_security_group_rule" "allow_http_inbound_test" {
  type = "ingress"
  security_group_id = aws_security_group.sg-terraform-test.id
  from_port = local.http_port
  to_port = local.http_port
  protocol = local.tcp_protocol
  cidr_blocks = local.all_ips
}

resource "aws_security_group_rule" "allow_http_outbound_test" {
  type = "egress"
  security_group_id = aws_security_group.sg-terraform-test.id
  from_port   = local.any_port
  to_port     = local.any_port
  protocol    = local.any_protocol
  cidr_blocks = local.all_ips
}

data "terraform_remote_state" "db" {
  backend = "s3"
  config = {
    bucket = var.db_remote_state_bucket
    key = var.db_remote_state_key
    region = "eu-west-1"
  }
}

terraform {
    backend "s3" {
        bucket = "garazas-terraform-state"
        key = var.asg_remote_state_key
        region = "eu-west-1"
        dynamodb_table = "terraform-locks"
        encrypt = true
    }
}
