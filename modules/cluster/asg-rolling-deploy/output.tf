output "db_address" {
  value = data.terraform_remote_state.db.outputs.address
  description = "DB address"
}

output "asg_name" {
  value = aws_autoscaling_group.terraform-asg.name
  description = "The name of ASG"
}

output "instance_security_group_id" {
  value = aws_security_group.sg-terraform-test.id
  description = "The ID of ASG SG"
}
