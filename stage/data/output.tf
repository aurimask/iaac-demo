output "address" {
    value = aws_db_instance.terraform_database.address
    description = "This is the address to connect"
}

output "port" {
    value = aws_db_instance.terraform_database.port
    description = "The port database is listening"
}