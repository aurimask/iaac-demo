provider "aws" {
  version = "~> 2.0"
  region ="eu-west-1"
}

locals {
  http_port = 80
  any_port = 0
  any_protocol = "-1"
  tcp_protocol = "tcp"
  all_ips = ["0.0.0.0/0"]
}

resource "aws_lb" "terraform-alb" {
  name = var.alb_name
  load_balancer_type = "application"
  subnets         = var.subnet_ids
  security_groups = [aws_security_group.sg-alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.terraform-alb.arn
  port = local.http_port
  protocol = "HTTP"
  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code = 404
    }
  }
}

resource "aws_security_group" "sg-alb" {
  name = var.alb_name
}

resource "aws_security_group_rule" "allow_http_inbound_alb" {
  type = "ingress"
  security_group_id = aws_security_group.sg-alb.id
  from_port = local.http_port
  to_port = local.http_port
  protocol = local.tcp_protocol
  cidr_blocks = local.all_ips
}

resource "aws_security_group_rule" "allow_http_outbound_alb" {
  type = "egress"
  security_group_id = aws_security_group.sg-alb.id
  from_port   = local.any_port
  to_port     = local.any_port
  protocol    = local.any_protocol
  cidr_blocks = local.all_ips
}

terraform {
    backend "s3" {
        bucket = "garazas-terraform-state"
        key = var.alb_remote_state_key
        region = "eu-west-1"
        dynamodb_table = "terraform-locks"
        encrypt = true
    }
}
