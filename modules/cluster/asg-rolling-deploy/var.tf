variable "instance_type" {
  description = "The type of EC2 instance"
  type = string
}

variable "cluster_name" {
  description = "The name to use for all the cluster resources"
  type = string
}

variable "subnet_ids" {
  description = "The subnet IDs to deploy to"
  type = list(string)
}

variable "target_group_arns" {
  description = "The ARNs of ELB target groups in which to register Instances"
  type = list(string)
  default = []
}

variable "health_check_type" {
  description = "The type of health check. EC2 or ELB"
  type = string
  default = "EC2"
}

variable "min_size" {
  description = "The minimum number of EC2 in ASG"
  type = number
}

variable "max_size" {
  description = "The maximum number of EC2 in ASG"
  type = number
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG"
  type = map(string)
  default = {}
}

variable "asg_remote_state_key" {
  description = "The path for the web remote state in S3"
  type = string
}

variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for the DB remote state"
  type = string
}

variable "db_remote_state_key" {
  description = "The path for the database's remote state in S3"
  type = string
}
