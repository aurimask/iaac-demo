module "asg" {
  source = "../../cluster/asg-rolling-deploy"
  cluster_name = "app-${var.environment}"
  instance_type = var.instance_type
  min_size = var.min_size
  max_size = var.max_size
  subnet_ids = data.aws_subnet_ids.default.ids
  target_group_arns = [aws_lb_target_group.tg-alb.arn]
  health_check_type = "ELB"
  custom_tags = var.custom_tags
  asg_remote_state_key ="${var.environment}/asg/terraform.tfstate"
  db_remote_state_bucket = "garazas-terraform-state"
  db_remote_state_key = "stage/data/terraform.tfstate"
}

module "alb" {
  source = "../../networking/alb"
  alb_name = "alb-${var.environment}"
  subnet_ids = data.aws_subnet_ids.default.ids
  alb_remote_state_key ="${var.environment}/alb/terraform.tfstate"
}

resource "aws_lb_target_group" "tg-alb" {
  name = "${var.environment}-tg-alb"
  port = var.server_port
  protocol = "HTTP"
  vpc_id = data.aws_vpc.default.id
  health_check {
    path = "/"
    protocol = "HTTP"
    matcher = "200"
    interval = 15
    timeout = 3
    healthy_threshold = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener_rule" "rule-alb" {
  listener_arn = module.alb.alb_http_listener_arn
  priority = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.tg-alb.arn
  }
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}


terraform {
    backend "s3" {
        bucket = "garazas-terraform-state"
        key = var.app_remote_state_key
        region = "eu-west-1"
        dynamodb_table = "terraform-locks"
        encrypt = true
    }
}
