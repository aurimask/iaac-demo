output "s3_bucket_arn" {
    value = aws_s3_bucket.garazas_terraform_state.arn
    description = "The ARN of a bucket"
}

output "dynamodb_table_name" {
  value = aws_dynamodb_table.terraform_locks.name
  description = "Name of the DynamoDB table"
}
