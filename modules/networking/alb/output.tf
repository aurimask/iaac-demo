output "alb_dns_name" {
  value = aws_lb.terraform-alb.dns_name
  description = "The domain name of the load balancer"
}

output "alb_http_listener_arn" {
  value = aws_lb_listener.http.arn
  description = "ARN of AWS LB listener"
}

output "alb_security_group_id" {
  value = aws_security_group.sg-alb.id
  description = "ID of ALB security group"
}
