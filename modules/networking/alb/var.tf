variable "alb_name" {
  type = string
  description = "ALB name"
}

variable "alb_remote_state_key" {
  description = "The path for the web remote state in S3"
  type = string
}

variable "subnet_ids" {
  description = "The subnet IDs to deploy to"
  type = list(string)
}
