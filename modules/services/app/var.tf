variable "environment" {
  description = "The name of the environment we are deploying to"
  type = string
}

variable "instance_type" {
  description = "The type of EC2 instance"
  type = string
}

variable "min_size" {
  description = "The minimum number of EC2 in ASG"
  type = number
}

variable "max_size" {
  description = "The maximum number of EC2 in ASG"
  type = number
}

variable "custom_tags" {
  description = "Custom tags to set on the Instances in the ASG"
  type = map(string)
  default = {}
}

variable "app_remote_state_key" {
  description = "The path for the web remote state in S3"
  type = string
}

variable "server_port" {
  description = "Server port"
  type = number
}
